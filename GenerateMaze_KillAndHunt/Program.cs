﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace GenerateMaze_KillAndHunt
{
    class Program
    {
        static char[,] maze;
        static void Main(string[] args)
        {
            for(int i = 0; i < 1000; i++)
            {
                Console.WriteLine("Creating " + i + " / " + 1000);
                InitMap();
                KillAndHunt();
                ShowMap();
                SaveMapToFile("maze_"+i);
            }
            
        }

        static void InitMap()
        {
            maze = new char[27, 21];
            for (int i = 0; i < 27; i++)
            {
                for (int j = 0; j < 21; j++)
                {
                    maze[i, j] = '#';
                }
            }
            //x vertical
            for (int i = 0; i < 27; i+=2)
            {
                for (int j = 0; j < 21; j++)
                {
                    maze[i, j] = '-';
                }
            }
            //x vertical
            for (int i = 1; i < 27; i += 2)
            {
                for (int j = 0; j < 21; j+= 2)
                {
                    maze[i, j] = '|';
                }
            }

        }
        static void KillAndHunt()
        {
            Point CurrentPoint = new Point { x = 1, y = 1 };
            int huntIndex = 1;
            while (true)
            {
                Kill(CurrentPoint);
                CurrentPoint = Hunt(ref huntIndex);
                if(CurrentPoint.x == -1 && CurrentPoint.y == -1)
                {
                    return;
                }
            }
        }
        static void ShowMap()
        {
            for(int i= 0; i < 27; i++)
            {
                for(int j = 0; j < 21; j++)
                {
                    Console.Write(maze[i,j]);
                }
                Console.Write('\n');
            }
            Console.Write('\n');
        }

        //-----------------------------------

        static Point Hunt(ref int index)
        {
            while (true)
            {
                for (int i = 1; i <= 20; i += 2)
                {
                    if (maze[index, i] == '#' && ((index + 2 <= 20 && maze[index+2, i] == ' ') || (index - 2 > 0 && maze[index - 2, i] == ' ')))
                    {
                        return new Point { x = i, y = index };
                    }
                }

                for (int i = 1; i <= 20; i += 2)
                {
                    if (maze[index, i] == '#')
                    {
                        return new Point { x = i, y = index };
                    }
                }

                index++;
                if(index >= 26)
                {
                    return new Point { x = -1, y = -1 };
                }
            }
        }
        static void Kill(Point CurrentPoint)
        {
            bool putEndPoint = CurrentPoint.x == 1 && CurrentPoint.y == 1;

            ConnectRoad(CurrentPoint);
            maze[CurrentPoint.y, CurrentPoint.x] = ' ';

            List<Char> preventActions = new List<Char>();
            bool result = true;

            while (true)
            {
                //ShowMap();
                //Console.ReadKey();
                Thread.Sleep(5);
                Char command = CreateRandomWalkCommand(ref preventActions);
                switch (command)
                {
                    case 'L':
                        result = KillLeft(ref CurrentPoint);
                        if (result)
                        {
                            preventActions.Clear();
                            preventActions.Add('R');
                        }
                        else
                        {
                            preventActions.Add('L');
                            continue;
                        }
                        break;
                    case 'T':
                        result = KillTop(ref CurrentPoint);
                        if (result)
                        {
                            preventActions.Clear();
                            preventActions.Add('B');
                        }
                        else
                        {
                            preventActions.Add('T');
                            continue;
                        }
                        break;
                    case 'R':
                        result = KillRight(ref CurrentPoint);
                        if (result)
                        {
                            preventActions.Clear();
                            preventActions.Add('L');
                        }
                        else
                        {
                            preventActions.Add('R');
                            continue;
                        }
                        break;
                    case 'B':
                        result = KillBottom(ref CurrentPoint);
                        if (result)
                        {
                            preventActions.Clear();
                            preventActions.Add('T');
                        }
                        else
                        {
                            preventActions.Add('B');
                            continue;
                        }
                        break;
                    case 'E':
                        if (putEndPoint)
                        {
                            maze[CurrentPoint.y, CurrentPoint.x] = 'E';
                        }
                        return;
                    default:
                        throw new Exception();
                }
            }
        }
        static char CreateRandomWalkCommand(ref List<Char> preventActions)
        {
            
            if(preventActions.IndexOf('L') >= 0 && preventActions.IndexOf('R') >= 0 && preventActions.IndexOf('T') >= 0 && preventActions.IndexOf('B') >= 0)
            {
                return 'E';
            }
            Random rnd = new Random();

            while (true)
            {
                int r = rnd.Next(1, 9);
                Char command = ' ';
                switch (r)
                {
                    case 1:
                        command = 'L';
                        break;
                    case 2:
                        command = 'T';
                        break;
                    case 3:
                        command = 'R';
                        break;
                    case 4:
                        command = 'B';
                        break;
                    case 5:
                        command = 'B';
                        break;
                    case 6:
                        command = 'B';
                        break;
                    case 7:
                        command = 'R';
                        break;
                    case 8:
                        command = 'R';
                        break;
                    default:
                        throw new Exception();
                }

                if (preventActions.IndexOf(command) >= 0)
                {
                    continue;
                }
                else
                {
                    return command;
                }
            }

            
        }
        static void ConnectRoad(Point currentPoint)
        {
            try
            {
                //left
                if (maze[currentPoint.y, currentPoint.x - 2] == ' ')
                {
                    maze[currentPoint.y, currentPoint.x - 1] = ' ';
                    return;
                }
                //right
                if (maze[currentPoint.y, currentPoint.x + 2] == ' ')
                {
                    maze[currentPoint.y, currentPoint.x + 1] = ' ';
                    return;
                }
                //top
                if (maze[currentPoint.y - 2, currentPoint.x] == ' ')
                {
                    maze[currentPoint.y - 1, currentPoint.x] = ' ';
                    return;
                }
                
                //bottom
                if (maze[currentPoint.y + 2, currentPoint.x] == ' ')
                {
                    maze[currentPoint.y + 1, currentPoint.x] = ' ';
                    return;
                }
                
            }
            catch
            {
                return;
            }
        }
        static bool KillLeft(ref Point currentPoint)
        {
            if(currentPoint.x <= 1)
            {
                return false;
            }
            if (maze[currentPoint.y, currentPoint.x - 1] == ' ' || maze[currentPoint.y, currentPoint.x - 2] == ' ')
            {
                return false;
            }
            else
            {
                maze[currentPoint.y, currentPoint.x - 1] = ' ';
                maze[currentPoint.y, currentPoint.x - 2] = ' ';
                currentPoint.x = currentPoint.x - 2;
                return true;
            }
        }
        static bool KillRight(ref Point currentPoint)
        {
            if (currentPoint.x >= 19)
            {
                return false;
            }
            if (maze[currentPoint.y, currentPoint.x + 1] == ' ' || maze[currentPoint.y, currentPoint.x + 2] == ' ')
            {
                return false;
            }
            else
            {
                maze[currentPoint.y, currentPoint.x + 1] = ' ';
                maze[currentPoint.y, currentPoint.x + 2] = ' ';
                currentPoint.x = currentPoint.x + 2;
                return true;
            }
        }
        static bool KillTop(ref Point currentPoint)
        {
            if (currentPoint.y <= 1)
            {
                return false;
            }
            if (maze[currentPoint.y - 1, currentPoint.x] == ' ' || maze[currentPoint.y - 2, currentPoint.x] == ' ')
            {
                return false;
            }
            else
            {
                maze[currentPoint.y - 1, currentPoint.x] = ' ';
                maze[currentPoint.y - 2, currentPoint.x] = ' ';
                currentPoint.y = currentPoint.y - 2;
                return true;
            }
        }
        static bool KillBottom(ref Point currentPoint)
        {
            if (currentPoint.y >= 25)
            {
                return false;
            }
            if (maze[currentPoint.y + 1, currentPoint.x] == ' ' || maze[currentPoint.y + 2, currentPoint.x] == ' ')
            {
                return false;
            }
            else
            {
                maze[currentPoint.y + 1, currentPoint.x] = ' ';
                maze[currentPoint.y + 2, currentPoint.x] = ' ';
                currentPoint.y = currentPoint.y + 2;
                return true;
            }
        }

        //-------------------------------------
        static void SaveMapToFile(string name)
        {
            string content = "";
            for (int i = 0; i < 27; i++)
            {
                for (int j = 0; j < 21; j++)
                {
                    content += maze[i, j];
                }
                content+='\n';
            }
            System.IO.Directory.CreateDirectory("Maze");
            System.IO.File.WriteAllText(@"Maze\" + name+".txt", content);
        }
    }

    class Point
    {
        public int x, y;
        public Point CreateRandomPoint(int limitX, int limitY)
        {
            Random rnd = new Random();
            return new Point
            {
                x = rnd.Next(1, limitX),
                y = rnd.Next(1, limitY)
            };
        }
    }
}
