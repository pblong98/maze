﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageSceneControler : MonoBehaviour
{
    [SerializeField] RectTransform viewPortContent;
    [SerializeField] GameObject stageButton;
    [SerializeField] GridLayoutGroup contentGrid;
    [SerializeField] ScrollRect scrollRect;
    [SerializeField] bool resetAndCreateHistory;
    [SerializeField] GameObject loading;
    int unlockedStage = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("UnLockedStage"))
        {
            GenerateFakeHistory();
        }
        unlockedStage = PlayerPrefs.GetInt("UnLockedStage");
        //Debug.Log(PlayerPrefs.GetInt("PlayStage"));
        contentGrid.cellSize = new Vector2(Screen.width / 5, Screen.width / 4);
        contentGrid.spacing = new Vector2(Screen.width / 20, Screen.width / 10);

        bool dirFlag = true;
        bool activeVerticalLinkFlag = false;
        for (int i = 1; i <= 999; i+=4)
        {
            CreateStageRowInMap(i, ref dirFlag, ref activeVerticalLinkFlag);
        }

        scrollRect.verticalNormalizedPosition = 0;
    }

    void CreateStageRowInMap(int start, ref bool isInsc, ref bool activeVerticalLinkFlag)
    {
        StageButton stageButtonIns;
        if (isInsc)
        {
            int _start = start;
            for (int i = 0; i < 4; i++)
            {
                stageButtonIns = (Instantiate(stageButton, Vector3.zero, Quaternion.Euler(Vector3.zero), viewPortContent) as GameObject).GetComponent<StageButton>();
                stageButtonIns.level = _start;
                stageButtonIns.isVisibleLinkH = (i==0);

                if(stageButtonIns.level <= unlockedStage)
                {
                    stageButtonIns.rates = PlayerPrefs.GetInt("Rate_" + stageButtonIns.level);
                    stageButtonIns.isLocked = false;
                }

                if (stageButtonIns.level == unlockedStage + 1)
                {
                    stageButtonIns.isLocked = false;
                }

                if (!activeVerticalLinkFlag)
                {
                    stageButtonIns.isVisibleLinkV = (i == 3);
                }
                else
                {
                    stageButtonIns.isVisibleLinkV = (i == 0);
                }
                _start++;
            }
        }
        else
        {
            int _start = start + 3;
            for (int i = 0; i < 4; i++)
            {
                stageButtonIns = (Instantiate(stageButton, Vector3.zero, Quaternion.Euler(Vector3.zero), viewPortContent) as GameObject).GetComponent<StageButton>();

                if (_start == 1000) //
                {
                    stageButtonIns.isInVisible = true;
                };

                stageButtonIns.level = _start;
                stageButtonIns.isVisibleLinkH = (i == 0);
                stageButtonIns.isVisibleLinkV = (!activeVerticalLinkFlag && i == 3);

                if (stageButtonIns.level <= unlockedStage)
                {
                    stageButtonIns.rates = PlayerPrefs.GetInt("Rate_" + stageButtonIns.level);
                    stageButtonIns.isLocked = false;
                }
                if (stageButtonIns.level == unlockedStage + 1)
                {
                    stageButtonIns.isLocked = false;
                }

                if (!activeVerticalLinkFlag)
                {
                    stageButtonIns.isVisibleLinkV = (i == 3);
                }
                else
                {
                    stageButtonIns.isVisibleLinkV = (i == 0);
                }
                
                _start--;
            }
        }
        isInsc = !isInsc;
        activeVerticalLinkFlag = !activeVerticalLinkFlag;
    }

    void GenerateFakeHistory()
    {
        int playStage = UnityEngine.Random.Range(1, 999);
        PlayerPrefs.SetInt("PlayStage", playStage);
        PlayerPrefs.SetInt("UnLockedStage", playStage);
        for (int i = 1; i <= playStage; i++)
        {
            PlayerPrefs.SetInt("Rate_"+i, UnityEngine.Random.Range(1, 4));
        }
        //Debug.Log(playStage);
    }
    public void ResetHistory()
    {
        PlayerPrefs.DeleteAll();
        if (!resetAndCreateHistory)
        {
            PlayerPrefs.SetInt("UnLockedStage", 0);
        }
        loading.SetActive(true);
        StartCoroutine(LoadAsync());
    }

    [SerializeField] Slider loadingSlider;

    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(1);
        while (!operation.isDone)
        {
            //Debug.Log(operation.progress);
            loadingSlider.value = operation.progress * 100;
            yield return null;
        }
    }
}
