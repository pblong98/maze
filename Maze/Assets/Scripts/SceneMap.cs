﻿using System.Collections.Generic;
using UnityEngine;


public class SceneMap : MonoBehaviour
{
    public static List<Node> Nodes;
    public static float[,] FloydMatrix;
    // Start is called before the first frame update
    private void Awake()
    {
        
    }

    private void Start()
    {
        Nodes = new List<Node>();
        //Find and init all node
        var nodeObjects = GameObject.FindGameObjectsWithTag("Node");
        //Debug.Log(nodeObjects.Length);
        foreach (var node in nodeObjects)
        {
            Nodes.Add(node.GetComponent<Node>());
        }
        FloydMatrix = FloydWarshall.GetInstance().FloydMatrix;
    }

    public static List<Node> Findway(Node start, Node dest)
    {
        List<Node> tempArr = new List<Node>();
        //Debug.Log(start + "  " + dest);
        Find(start, FloydWarshall.nodes.IndexOf(dest), tempArr);
        return tempArr;
    }

    public static Node GetClosestNodeWithPosition(Vector2 pos)
    {
        float mindistance = Vector2.Distance(Nodes[0].transform.position, pos);
        Node tempnode = Nodes[0];
        foreach (var item in Nodes)
        {
            float tempDis = Vector2.Distance(item.transform.position, pos);
            if (tempDis < mindistance)
            {
                mindistance = tempDis;
                tempnode = item.GetComponent<Node>();
            }
        }
        return tempnode;
    }

    static void Find(Node node, int destIndex, List<Node> list)
    {
        if (FloydWarshall.nodes.IndexOf(node) == destIndex || node.RelativeNode == null || node.RelativeNode.Count == 0)
        {
            list.Add(node);
            return;
        }

        Node tempNode = node.RelativeNode[0];
        //Debug.Log(FloydWarshall.nodes.IndexOf(node.RelativeNode[0]) + "   " + destIndex);
        float mindistance = FloydMatrix[FloydWarshall.nodes.IndexOf(node.RelativeNode[0]), destIndex];

        foreach (var i in node.RelativeNode)
        {
            int index = FloydWarshall.nodes.IndexOf(i);
            if (FloydMatrix[index, destIndex] < mindistance)
            {
                mindistance = FloydMatrix[index, destIndex];
                tempNode = i;
            }
        }
        list.Add(node);
        //Debug.Log(tempNode);
        Find(tempNode, destIndex, list);
    }

}
