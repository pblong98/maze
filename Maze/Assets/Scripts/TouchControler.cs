﻿using UnityEngine;

public class TouchControler : MonoBehaviour
{
    [SerializeField] Player player;
    public void Up()
    {
        player.IsMoveTop = true;
    }
    public void _Up()
    {
        player.IsMoveTop = false;
    }

    public void Down()
    {
        player.IsMoveBottom = true;
    }
    public void _Down()
    {
        player.IsMoveBottom = false;
    }

    public void Left()
    {
        player.IsMoveLeft = true;
    }
    public void _Left()
    {
        player.IsMoveLeft = false;
    }

    public void Right()
    {
        player.IsMoveRight = true;
    }
    public void _Right()
    {
        player.IsMoveRight = false;
    }

}
