﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndPoint : MonoBehaviour
{
    [SerializeField]
    MazeControler mazeControler;
    private void Start()
    {
        mazeControler = FindObjectOfType<MazeControler>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            //Debug.Log(MazeControler.TimeCount);
            int rate = 3;
            if(MazeControler.TimeCount > 41)
            {
                rate = 3;
            }
            if (MazeControler.TimeCount > 22 && MazeControler.TimeCount < 40)
            {
                rate = 2;
            }
            if (MazeControler.TimeCount > 0 && MazeControler.TimeCount < 21)
            {
                rate = 1;
            }
            PlayerPrefs.SetInt("Rate_" + PlayerPrefs.GetInt("PlayStage"), rate);
            if (PlayerPrefs.GetInt("PlayStage") > PlayerPrefs.GetInt("UnLockedStage"))
            {
                PlayerPrefs.SetInt("UnLockedStage", PlayerPrefs.GetInt("PlayStage"));
            }
            mazeControler.NextState();

        }
    }
}
