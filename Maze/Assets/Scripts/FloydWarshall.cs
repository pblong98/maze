﻿using System.Collections.Generic;
using UnityEngine;

public class FloydWarshall
{
    private static FloydWarshall Instance;
    public static FloydWarshall GetInstance()
    {
        //if (Instance != null)
        //{
        //    return Instance;
        //}
        //else
        //{
        //    Instance = new FloydWarshall();
        //    return Instance;
        //}
        Instance = new FloydWarshall();
        return Instance;
    }

    private float[,] _FloydMatrix;
    public float[,] FloydMatrix
    {
        get
        {
            return _FloydMatrix;
        }
    }
    int n = 0;
    public static List<Node> nodes;
    private FloydWarshall()
    {
        nodes = new List<Node>();
        n = SceneMap.Nodes.Count;
        var gnodes = SceneMap.Nodes;
        foreach (var i in gnodes)
        {
            nodes.Add(i.GetComponent<Node>());
        }
        _FloydMatrix = new float[n, n];
        initFloydMatrix();
        CalculateMinPath();
    }

    private void initFloydMatrix()
    {
        for (int c = 0; c < n; c++)
        {
            for (int r = 0; r < n; r++)
            {
                if (nodes[c] == nodes[r])
                {
                    _FloydMatrix[c, r] = 0;
                }
                else
                {
                    if (nodes[c].RelativeNode.IndexOf(nodes[r]) == -1)
                    {
                        _FloydMatrix[c, r] = Mathf.Infinity;
                    }
                    else
                    {
                        _FloydMatrix[c, r] = GetDistanceBetween2Node(nodes[c], nodes[r]);
                    }
                }
            }
        }
    }
    float GetDistanceBetween2Node(Node a, Node b)
    {
        return Vector2.Distance(a.transform.position, b.transform.position);
    }
    void CalculateMinPath()
    {
        for (int k = 0; k < n; k++)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    _FloydMatrix[i, j] = Mathf.Min(_FloydMatrix[i, j], _FloydMatrix[i, k] + _FloydMatrix[k, j]);
                }
            }
        }
    }

    public void DebugMatrix()
    {
        for (int c = 0; c < n; c++)
        {
            for (int r = 0; r < n; r++)
            {
                Debug.Log(_FloydMatrix[c, r]);
            }
        }
    }
}
