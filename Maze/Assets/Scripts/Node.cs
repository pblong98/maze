﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Node : MonoBehaviour
{
    public List<Node> RelativeNode = new List<Node>();

#if (UNITY_EDITOR)
    private void Update()
    {
        if (RelativeNode.Count > 0)
        {
            foreach (var item in RelativeNode)
            {
                if (item.RelativeNode.IndexOf(this) != -1)
                {
                    //Debug.Log("B");
                    Debug.DrawLine(transform.position, item.transform.position, Color.green);
                }
                else
                {
                    //Debug.Log("a");
                    Debug.DrawLine(transform.position, item.transform.position, Color.red);
                }
            }
        }
    }
#endif
}
