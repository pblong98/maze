﻿using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    public bool IsMoveLeft, IsMoveRight, IsMoveTop, IsMoveBottom;
    public float Speed;
    Rigidbody2D rigid;
    [HideInInspector] public List<Node> route;
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        oldPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        AutoMove();
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        KeyBoardControl();
#endif
    }

    void Move()
    {
        if (IsMoveLeft)
        {
            rigid.velocity = new Vector2(-Speed, 0);
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        if (IsMoveRight)
        {
            rigid.velocity = new Vector2(Speed, 0);
            transform.rotation = Quaternion.Euler(0, 0, -90);
        }
        if (IsMoveTop)
        {
            rigid.velocity = new Vector2(0, Speed);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (IsMoveBottom)
        {
            rigid.velocity = new Vector2(0, -Speed);
            transform.rotation = Quaternion.Euler(0, 0, 180);
        }
    }

    void KeyBoardControl()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            IsMoveTop = true;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            IsMoveLeft = true;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            IsMoveBottom = true;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            IsMoveRight = true;
        }

        //

        if (Input.GetKeyUp(KeyCode.W))
        {
            IsMoveTop = false;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            IsMoveLeft = false;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            IsMoveBottom = false;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            IsMoveRight = false;
        }
    }

    void AutoMove()
    {
        if (route.Count > 0)
        {
            float step = Speed * Time.deltaTime; //
            transform.position = Vector3.MoveTowards(transform.position, route[0].transform.position, step);

            if (Vector3.Distance(transform.position, route[0].transform.position) < 0.001f)
            {
                route.RemoveAt(0);
                if (route.Count == 0)
                {
                    return;
                }
            }
            AutoRotation();
        }
    }
    private Vector2 oldPos;
    void AutoRotation()
    {
        if (oldPos.x > transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        if (oldPos.x < transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, -90);
        }
        if (oldPos.y < transform.position.y)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (oldPos.y > transform.position.y)
        {
            transform.rotation = Quaternion.Euler(0, 0, 180);
        }
        oldPos = transform.position;
    }

}
