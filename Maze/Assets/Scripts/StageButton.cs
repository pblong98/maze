﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageButton : MonoBehaviour
{
    public int level;
    public int rates;
    [SerializeField] GameObject lockedLayer;
    [SerializeField] GameObject darkLayer;
    [SerializeField] GameObject tutorial;
    [SerializeField] GameObject[] stars;
    [SerializeField] Text label;
    public bool isLocked;
    public bool isVisibleLinkH, isVisibleLinkV, isInVisible;
    [SerializeField] GameObject linkH, linkV;

    static float linkHScaleX, linkVScaleY, screenwidth;
    static bool linkScaleCalculated = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!linkScaleCalculated)
        {
            screenwidth = Screen.width;
            linkHScaleX = screenwidth * linkH.transform.localScale.x / 4f;
            linkVScaleY = screenwidth * linkV.transform.localScale.x / 4f;
            linkScaleCalculated = true;
            //Debug.Log('s');
        }

        if (isInVisible)
        {
            linkH.SetActive(false);
            linkV.SetActive(false);
            label.gameObject.SetActive(false);
            lockedLayer.SetActive(false);
            darkLayer.SetActive(false);
            tutorial.SetActive(false);
            stars[0].SetActive(false);
            stars[1].SetActive(false);
            stars[2].SetActive(false);
            Destroy(GetComponent<Image>());
                return;
        }

        label.text = level.ToString();
        if (rates > 0)
        {
            //active rate stars
            if (!isLocked)
            {
                for (int i = 0; i < rates; i++)
                {
                    stars[i].SetActive(true);
                }
            }
        }
        if (isLocked)
        {
            darkLayer.gameObject.SetActive(true);
            lockedLayer.gameObject.SetActive(true);
        }
        else
        {
            darkLayer.gameObject.SetActive(false);
            lockedLayer.gameObject.SetActive(false);
        }
        linkH.SetActive(isVisibleLinkH);
        linkV.SetActive(isVisibleLinkV);
        if (isVisibleLinkH)
        {
            linkH.transform.localScale = new Vector2(linkHScaleX, screenwidth / 10f);
            linkH.transform.localPosition = new Vector2((screenwidth / 3f), 0);
        }
        if (isVisibleLinkV)
        {
            linkV.transform.localScale = new Vector2(screenwidth / 10f, linkVScaleY);
            linkV.transform.localPosition = new Vector2(0, (screenwidth / 6f));
        }

        if(level == 1)
        {
            label.text = "";
            lockedLayer.gameObject.SetActive(false);
            darkLayer.gameObject.SetActive(false);
            tutorial.gameObject.SetActive(true);
        }
        this.GetComponent<Button>().onClick.AddListener(ButtonClicked);
    }

    public void ButtonClicked()
    {
        //Debug.Log(level);
        if(!isLocked)
        {
            PlayerPrefs.SetInt("PlayStage", level);
            SceneManager.LoadScene(2);
        }
    }

}
