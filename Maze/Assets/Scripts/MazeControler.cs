﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MazeControler : MonoBehaviour
{
    [SerializeField] float WallWidth, WallHeight;
    public GameObject Loading;
    [SerializeField] GameObject Wall, EndPoint, Node, touchControl;
    char[,] mazeSource;
    [SerializeField] List<Node> Nodes;
    const float PIXEL_UNIT = 0.0999942f;
    [SerializeField] Node StartNode, EndNode;
    public Player player;
    [SerializeField] LineRenderer hintLine;
    [SerializeField] Text title;
    //-----------------------------
    [SerializeField] Slider timeSlider;
    public static float TimeCount;
    float StartTime;
    private void Awake()
    {
        Nodes.Clear();
        //Load maze from file txt
        mazeSource = new char[27, 21];
        string source = (Resources.Load("maze_"+ PlayerPrefs.GetInt("PlayStage")) as TextAsset).text;
        int loadSourceIndex = 0;
        for (int i = 0; i < 27; i++)
        {
            for (int j = 0; j < 21; j++)
            {
                if(source[loadSourceIndex] == '\n')
                {
                    loadSourceIndex++;
                }
                mazeSource[i, j] = source[loadSourceIndex];
                loadSourceIndex++;
            }
        }
        //Build maze
        Vector2 spawPos = new Vector2(0, 0);
        for (int i = 0; i < 27; i++)
        {
            bool isWallRowBluiding = (i % 2 == 0);
            for (int j = 0; j < 21; j++)
            {
                if (isWallRowBluiding)
                {
                    bool isWallCrossRoad = (j % 2 == 0);
                    if (isWallCrossRoad && mazeSource[i, j] == '-')
                    {
                        Vector2 insPos = new Vector2(spawPos.x + PIXEL_UNIT * WallHeight / 2, spawPos.y);
                        Transform wall = (Instantiate(Wall, insPos, Wall.transform.rotation) as GameObject).transform;
                        wall.localScale = new Vector2(WallHeight, WallHeight);
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallHeight, spawPos.y);

                    }
                    if (isWallCrossRoad && mazeSource[i, j] == ' ')
                    {
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallHeight, spawPos.y);
                    }

                    if (!isWallCrossRoad && mazeSource[i, j] == '-')
                    {
                        Vector2 insPos = new Vector2(spawPos.x + PIXEL_UNIT * WallWidth / 2, spawPos.y);
                        Transform wall = (Instantiate(Wall, insPos, Wall.transform.rotation) as GameObject).transform;
                        wall.localScale = new Vector2(WallWidth, WallHeight);
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallWidth, spawPos.y);
                    }

                    if (!isWallCrossRoad && mazeSource[i, j] == ' ')
                    {
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallWidth, spawPos.y);
                    }
                }
                else
                {
                    bool isWallCell = (j % 2 == 0);
                    if (isWallCell && mazeSource[i, j] == '|')
                    {
                        Vector2 insPos = new Vector2(spawPos.x + PIXEL_UNIT * WallHeight / 2, spawPos.y);
                        Transform wall = (Instantiate(Wall, insPos, Quaternion.Euler(0,0,90)) as GameObject).transform;
                        wall.localScale = new Vector2(WallWidth, WallHeight);
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallHeight, spawPos.y);

                    }

                    if (isWallCell && mazeSource[i, j] == ' ')
                    {
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallHeight, spawPos.y);
                    }

                    //player can walking in this cells
                    if (!isWallCell && (mazeSource[i, j] == ' ' || mazeSource[i, j] == 'E'))
                    {
                        Vector2 insPos = new Vector2(spawPos.x + PIXEL_UNIT * WallWidth / 2, spawPos.y);
                        if (mazeSource[i, j] == 'E')
                        {
                            Transform endPoint = (Instantiate(EndPoint, insPos, EndPoint.transform.rotation) as GameObject).transform;
                            endPoint.name = i + "," + j;
                            var endNode = endPoint.gameObject.GetComponent<Node>();
                            Nodes.Add(endNode);
                            EndNode = endNode;
                        }
                        else
                        {
                            Transform node = (Instantiate(Node, insPos, Node.transform.rotation) as GameObject).transform;
                            node.name = i + "," + j;
                            Nodes.Add(node.gameObject.GetComponent<Node>());
                        }
                        spawPos = new Vector2(spawPos.x + PIXEL_UNIT * WallWidth, spawPos.y);
                    }
                }
            }
            spawPos = new Vector2(0, spawPos.y - (PIXEL_UNIT * WallWidth / 2) - (PIXEL_UNIT * WallHeight / 2));
        }
        StartNode = GameObject.Find("1,1").GetComponent<Node>();
        //Create node's network
        foreach(var node in Nodes)
        {
            int i = int.Parse(node.name.Split(',')[0]);
            int j = int.Parse(node.name.Split(',')[1]);
            //Find left node
            if (j - 2 > 0)
            {
                if (mazeSource[i,j-1] == ' ')
                {
                    node.RelativeNode.Add(GameObject.Find(i+","+(j-2)).GetComponent<Node>());
                }
            }
            //Find right node
            if (j + 2 < 20)
            {
                if (mazeSource[i, j + 1] == ' ')
                {
                    node.RelativeNode.Add(GameObject.Find(i + "," + (j + 2)).GetComponent<Node>());
                }
            }
            //Find top node
            if (i - 2 > 0)
            {
                if (mazeSource[i - 1, j] == ' ')
                {
                    node.RelativeNode.Add(GameObject.Find(i - 2 + "," + j).GetComponent<Node>());
                }
            }
            //Find bottom node
            if (i + 2 < 26)
            {
                if (mazeSource[i + 1, j] == ' ')
                {
                    node.RelativeNode.Add(GameObject.Find(i + 2 + "," + j).GetComponent<Node>());
                }
            }
        }
    }

    private void Start()
    {
        hintLine.transform.position = Vector2.zero;
        hintLine.gameObject.SetActive(false);
        StartTime = Time.time;
        title.text = "STAGE " + PlayerPrefs.GetInt("PlayStage");
        if(PlayerPrefs.GetInt("PlayStage") != 1)
        {
            KillTutorialAnimation();
        }
        else
        {
            Invoke("KillTutorialAnimation", 3);
        }
    }

    public void KillTutorialAnimation()
    {
        Destroy(touchControl.GetComponent<Animator>());
    }

    public void ShowHint()
    {
        hintLine.gameObject.SetActive(!hintLine.gameObject.active);
        var path = SceneMap.Findway(SceneMap.GetClosestNodeWithPosition(player.transform.position), EndNode);
        Debug.Log(SceneMap.Nodes.Count);

        Vector3[] positions = new Vector3[path.Count];
        for(int i = 0; i < path.Count; i++)
        {
            positions[i] = path[i].transform.position;
        }
        hintLine.positionCount = positions.Length;
        hintLine.SetPositions(positions);
    }

    public void AutoMove()
    {
        hintLine.gameObject.SetActive(true);
        var path = SceneMap.Findway(SceneMap.GetClosestNodeWithPosition(player.transform.position), EndNode);
        Vector3[] positions = new Vector3[path.Count];
        for (int i = 0; i < path.Count; i++)
        {
            positions[i] = path[i].transform.position;
        }
        hintLine.positionCount = positions.Length;
        hintLine.SetPositions(positions);
        player.route = path;
    }

    public void Back()
    {
        StartCoroutine(LoadAsync(1));
        Loading.SetActive(true);
    }

    public void NextState()
    {
        PlayerPrefs.SetInt("PlayStage", PlayerPrefs.GetInt("PlayStage")+1);
        StartCoroutine(LoadAsync(2));
        Loading.SetActive(true);
    }


    [SerializeField] Slider loadingSlider;

    IEnumerator LoadAsync(int scene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);
        while (!operation.isDone)
        {
            //Debug.Log(operation.progress);
            loadingSlider.value = operation.progress * 100;
            yield return null;
        }
    }

    private void Update()
    {
        timeSlider.value = 60 - (Time.time - StartTime);
        TimeCount = timeSlider.value;
    }

}
