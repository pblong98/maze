﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{
    [SerializeField]
    Slider slider;

    private void Start()
    {
        LoadLevel();
    }

    public void LoadLevel()
    {
        StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(1);
        while (!operation.isDone)
        {
            Debug.Log(operation.progress);
            slider.value = operation.progress * 100;
            yield return null;
        }
    }
}
